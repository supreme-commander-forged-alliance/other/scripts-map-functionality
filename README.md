# Script Snippets

A collection of scripts for oddly specific tasks.

### Cliff building

![](images/cliffbuilding.jpg)

#### Reasoning

Cliff building is a common technique where you build a factory on top of a cliff or down into a valley. It allows a player to quickly get presence in an area that would otherwise require a transport or a large detour. From a player perspective it may be hard to determine where these build locations are available. A consequence of that is frustration when a player gets it wrong.

The aim of this script is to create visual cues for both the (tech 1) engineer and the factory. If both are correct then the (tech 1) engineer should be able to build the factory after it moved towards the visual cue for the engineer.

#### Guidelines

The intent is that a tech 1 engineer can perform the cliff building. Please make sure that your map adheres to this as player will expect this in the future.

#### How to make it work

There are two unit groups as part of the army `NEUTRAL_CIVILIAN` that help assist creating the markings:
 - `CLIFFBUILD`: where you expect a factory to be buildable. 
 - `CLIFFENGINEER`: where you expect an (tech 1) engineer to be to build the factory.

Use land factories for the `CLIFFBUILD` unit group and tech 1 power generators for the `CLIFFENGINEER` unit group. For a practical example you can download the map Kaali from the vault.

To use the script copy the content of `CliffBuilding.lua` into your `%MAP_NAME%_script.lua` file. Then replace `%MAP_NAME%` with the name of the folder of your map. Prepend it at the top of the file. Then add the following bit of code to the `OnStart` function in your script file:

```lua
function OnStart()
    -- -- copy this bit -- --
    -- prepare the cliff build spots
    pcall(MarkLocations)
    -- --               -- --
end
```