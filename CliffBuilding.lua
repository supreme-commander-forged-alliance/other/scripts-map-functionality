
-- Script written by (Jip) Willem Wijnia

local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

-- Marks the locations for cliff building. Read the readme file for more instructions.
function MarkLocations()

    -- Provides a burst of vision. Decals are made on a per-player 
    -- basis so that they do not show up through the fog of war. This 
    -- burst of vision ensures that everyone sees the decals in question.
    function BurstVision(unit)
        local position = unit.Position

        local armies = ListArmies()
        for k, army in ListArmies() do 
            local brain = GetArmyBrain(army)
            ScenarioFramework.CreateVisibleAreaLocation(10, position, 1, brain)
        end
    end

    -- Flattens the area. It doesn't work that well in practice. Can 
    -- be used for testing.
    function FlattenArea(unit)

        -- retrieve skirt size
        local bp = GetUnitBlueprintByName(unit.type)
        local skirtX = bp.Footprint.SizeX
        local skirtZ = bp.Footprint.SizeZ

        -- retrieve position
        local position = unit.Position

        -- flatten it
        FlattenMapRect(
            position[1] - skirtX, 
            position[3] - skirtZ, 
            2 * skirtX, 2 * skirtZ - 1, 
            position[2]
        )
    end

    -- Finds entities that may block the building from being build. 
    -- Can be used for testing. You should always test it manually.
    function FindEntities(unit)

        local function MarkEntities(ents)
            while true do 
                WaitSeconds(0.1)
                for k, ent in ents do 
                    DrawCircle(ent:GetPosition(), 2, "ff0000")
                end
            end
        end

        -- retrieve skirt size
        local bp = GetUnitBlueprintByName(unit.type)
        local skirtX = bp.Physics.SkirtSizeX
        local skirtZ = bp.Physics.SkirtSizeZ

        -- retrieve position
        local position = unit.Position

        -- construct the rectangle of the skirt
        local rect = Rect(
            position[1] - 0.5 * skirtX, 
            position[3] - 0.5 * skirtZ,  
            position[1] + 0.5 * skirtX, 
            position[3] + 0.5 * skirtZ
        )

        -- destroy all entities in the rectangle
        local ents = GetReclaimablesInRect(rect)
        if ents then 

            local n = table.getn(ents)
            if n > 0 then 
                WARN("Up to " .. n .. " entities should be removed from " .. repr(rect))
            end

            ForkThread(MarkEntities, ents)
        end
    end

    -- Constructs the decal where the size depends on the size of the unit.
    function ConstructDecal(unit)

        -- retrieve skirt size
        local bp = GetUnitBlueprintByName(unit.type)
        local skirtX = bp.Physics.SkirtSizeX
        local skirtZ = bp.Physics.SkirtSizeZ

        -- retrieve position
        local position = unit.Position

        -- construct the decal
        CreateDecal(
            position, 
            0,
            "/textures/highlight_bracket_neutral_sm.dds",
            "",
            "Albedo",
            skirtX,
            skirtZ,
            400,
            -1,
            "NEUTRAL_CIVILIAN",
            1
        )
    end 

    -- retrieve places we want to cliff build
    local scenario = ScenarioInfo.Env.Scenario 
    local tblNode = scenario.Armies["NEUTRAL_CIVILIAN"].Units.Units
    local facs = tblNode["CLIFFBUILD"].Units  
    local engs = tblNode["CLIFFENGINEER"].Units  

    -- make them cliff build friendly
    if facs then 
        for k, unit in facs do
            ConstructDecal(unit)
            BurstVision(unit)

            -- for testing
            -- FlattenArea(unit)
            -- FindEntities(unit)
        end
    end

    if engs then 
        for k, unit in engs do 
            ConstructDecal(unit)
            BurstVision(unit)

            -- for testing
            -- FlattenArea(unit)
            -- FindEntities(unit)
        end
    end
end